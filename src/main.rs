mod lib;
mod defaults;

use crate::{
    lib::{
        BluePrint,
        Errors,
        TemplateCommand,
    },
    defaults::{
        register_macros,
        register_commands,
        register_auto_commands,
        register_keywords,
    }
};
use std::{
    time::{SystemTime, UNIX_EPOCH},
    path::PathBuf,
    fs::File,
    io::{Read, Write},
    process::Command,
    collections::HashMap
};
use structopt::StructOpt;
use toml::value::Map;
use serde_derive::Deserialize;

#[derive(StructOpt)]
#[structopt(name = "ssc")]
struct Options {
    /// specifies input file
    #[structopt(short, long, name = "INPUT FILE", parse(from_os_str))]
    input: PathBuf,
    /// specifies output file
    #[structopt(short,long, name = "OUTPUT FILE", parse(from_os_str))]
    output: PathBuf,
    /// load commandset from template file
    #[structopt(short,long, name = "template file", parse(from_os_str))]
    template: Option<PathBuf>,

    // toggle features
    /// enable macros
    #[structopt(long = "no-macros", parse(from_flag = std::ops::Not::not))]
    use_macros: bool,
    /// enable commands
    #[structopt(long = "no-commands", parse(from_flag = std::ops::Not::not))]
    use_commands: bool,
    /// enable automaic command generation
    #[structopt(long = "no-auto-commands", parse(from_flag = std::ops::Not::not))]
    use_auto_commands: bool,
    /// enable keywords
    #[structopt(long = "no-keywords", parse(from_flag = std::ops::Not::not))]
    use_keywords: bool,
    /// disable auto progn
    #[structopt(long = "no-autoprogn", parse(from_flag = std::ops::Not::not))]
    use_autoprogn: bool,

    /// run script on completion
    #[structopt(short, long = "command")]
    cmd: Option<PathBuf>,

    /// enable time logging
    #[structopt(short, long)]
    verbose: bool,
}

#[derive(Deserialize)]
struct TemplateData {
    /// argument count
    len: usize,
    /// template
    template: String
}

fn main() {
    let options = Options::from_args();

    let start = SystemTime::now();
    let begin = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");

    let mut blueprint = BluePrint::new();

    // register functions, as proposed by script
    if options.use_macros {
        register_macros(&mut blueprint);
    }
    if options.use_commands {
        register_commands(&mut blueprint);
    }
    if options.use_auto_commands {
        register_auto_commands(&mut blueprint);
    }
    if options.use_keywords {
        register_keywords(&mut blueprint);
    }

    // read template file
    if let Some(path) = options.template {
        let mut template = String::new();
        if let Ok(mut file) = File::open(path) {
            match file.read_to_string(&mut template) {
                Ok(_) => {
                    // parse toml
                    let cmds: HashMap<String, TemplateData>= toml::from_str(&template).unwrap();
                    for (name, dt) in cmds.iter() {
                        blueprint.register_templated_command(&TemplateCommand {
                            name: name.to_string(),
                            len: dt.len,
                            template: (&dt.template).to_string()
                        });
                    }
                }
                Err(e) => {
                    println!("Failed to read template file");
                    return;
                }
            }
        }
    }

    if let Ok(mut file) = File::open(options.input) {
        let mut script = String::new();
        match file.read_to_string(&mut script) {
            Ok(_) => {
                if options.use_autoprogn {
                    script = format!("(progn {})", script);
                }
                match blueprint.compile(&script) {
                    Ok(dt) => {
                        // write file
                        if let Ok(mut of) = File::create(options.output) {
                            match of.write_all(format!("{}\n",dt).as_bytes()) {
                                Ok(_) => {
                                    // meassure time
                                    let pt = SystemTime::now();
                                    let end = pt
                                        .duration_since(UNIX_EPOCH)
                                        .expect("Time went backwards");
                                    if options.verbose {
                                        println!("{}", dt);
                                        println!("Compilation duration: {:?}", end - begin);
                                    }

                                    // run command
                                    if let Some(cmd) = options.cmd {
                                        let out = Command::new("sh")
                                            .arg("-c")
                                            .arg(cmd)
                                            .output()
                                            .expect("Failed to run callback command");
                                        if options.verbose {
                                            println!("Callback Output:\n{}", String::from_utf8(out.stdout).unwrap());
                                        }
                                    }
                                },
                                Err(_) => {
                                    println!("Couldn't write to output file");
                                }
                            }
                        } else {
                            println!("Couldn't create output file");
                        }
                    },
                    Err(e) => println!("{}", match e {
                        Errors::Custom(s) => s,
                        Errors::InvalidType => "Found invalid type".to_string(),
                        Errors::NotEnoughArguments => "Not enough arguments".to_string(),
                        Errors::TooManyArguments => "Too many arguments were given".to_string(),
                        Errors::Unknown => "An unknown eror occurred".to_string()
                    })
                }


            },
            Err(_) => println!("Couldn't read file")
        }
    } else {
        println!("Couldn't open file");
    }
}
