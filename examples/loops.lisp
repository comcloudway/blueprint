(dotimes 5
  (print i))

(let c 0)
(dowhile (< c 5)
         (print c)
         (set c (+ c 1)))


(loop
  (print 5)
  (break)
  )

(loop
  repeat 3 do
  (print i))
(loop
  repeat 3 collect
  (print i))

(let a (arr! 1 2 3))
(loop
  for x in a do
  (print x))
(loop
  for x on a collect
  (print x))
(loop
  for x from 10 to 3 do
  (print x))
(loop
  for x from 3 to 5 do
  (print x))
