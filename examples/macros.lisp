; arrays
(const a (arr! 5 4 3))
(print (get! a 1))
(multiple-values-bind 
  (d b c) 
  a)

; strings
(print (format! "num: {}" (get! a 1)))

; objects
(const p (obj!
           :firstname   "John"
           :lastname    "Doe"
           :id          5))
(print (get! p "id"))
(object-values-bind (firstname lastname) p)
