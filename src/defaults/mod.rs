pub mod auto;
pub mod commands;
pub mod keywords;
pub mod macros;

pub use auto::register_auto_commands;
pub use commands::register_commands;
pub use keywords::register_keywords;
pub use macros::register_macros;
