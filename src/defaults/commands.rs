use crate::lib::{BluePrint, Errors, ParserTypes};

/// registers some default commands
pub fn register_commands(blueprint: &mut BluePrint) {
    blueprint.register_command("make.instance", Box::new(|s, args| {
        if let Some(c) = args.get(1) {
            if let ParserTypes::Symbol(class_name) = &**c {
                let mut builder = String::new();

                let mut index = 2;
                while let Some(arg) = args.get(index) {
                    index+=1;
                    if let Ok(dt) = s.resolve(arg) {
                        builder.push_str(&dt);
                        builder.push(',');
                    }
                }
                builder.pop();

                return Ok(format!("new {}({})", class_name, builder));
            }
        }
        Err(Errors::InvalidType)
    }));
    blueprint.register_command("progn", Box::new(|s, args| {
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut buffer = String::new();
        let mut index = 1;
        while let Some(item) = args.get(index) {
            index+=1;
            match s.resolve(item) {
                Ok(dt) => {
                    buffer.push_str(&dt);
                    if index < args.len() && buffer.len() > 0 {
                        match buffer.chars().last() {
                            Some(c) => {
                                // skip adding semicolon if line is empty
                                if c == '\n' {
                                    continue;
                                }
                            },
                            None => {}
                        }
                        buffer.push_str(";\n");
                    }
                },
                Err(e) => return Err(e)
            }
        }

        Ok(buffer)
    }));
    blueprint.register_command("set", Box::new(|s, args| {
        if args.len() > 3 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut key = String::new();
        let mut value = String::new();

        if let Some(k) = args.get(1) {
            match s.resolve(k) {
                Ok(dt) => key.push_str(&dt),
                Err(e) => return Err(e)
            }
        }
        if let Some(v) = args.get(2) {
            match s.resolve(v) {
                Ok(dt) => value.push_str(&dt),
                Err(e) => return Err(e)
            }
        }

        Ok(format!("{} = {}", key, value))
    }));
    blueprint.register_command("values", Box::new(|s, args| {
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut builder = String::new();
        let mut index = 1;
        while let Some(f) = args.get(index) {
            index+=1;
            match s.resolve(f) {
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push(',')
                    }
                },
                Err(e) => return Err(e)
            }
        }
        Ok(format!("return [{}]", builder))
    }));

    // loops
    blueprint.register_command("dotimes", Box::new(|s, args| {
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments);
        }

        let mut count = String::new();
        let mut code = String::new();

        if let Some(c) = args.get(1) {
            match s.resolve(c) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    count.push_str(&dt);
                }
            }
        }
        let mut index = 2;
        while let Some(line) = args.get(index) {
            index+=1;
            match s.resolve(line) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    code.push_str(&dt);
                    if index < args.len() + 1 {
                        code.push_str(";\n");
                    }
                }
            }
        }

        Ok(format!("for (let i = 0; i<{}; i++) {{\n{}}}", count, code))
    }));
    blueprint.register_command("dowhile", Box::new(|s, args| {
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments);
        }

        let mut expr = String::new();
        let mut code = String::new();

        if let Some(c) = args.get(1) {
            match s.resolve(c) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    expr.push_str(&dt);
                }
            }
        }
        let mut index = 2;
        while let Some(line) = args.get(index) {
            index+=1;
            match s.resolve(line) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    code.push_str(&dt);
                    if index < args.len() + 1 {
                        code.push_str(";\n");
                    }
                }
            }
        }

        Ok(format!("while ({}) {{\n{}}}", expr, code))
    }));
    blueprint.register_command("loop", Box::new(|s, args| {
        let mut code = String::new();
        let mut is_basic = true;

        if let Some(a) = args.get(1) {
            match &**a {
                ParserTypes::Variable(dt) => {
                        if dt == "repeat" || dt == "for" {
                            is_basic = false;
                    }
                }
                _ => ()
            }
        }

        let mut index = 1;
        if is_basic {
            while let Some(item) = args.get(index) {
                index = index + 1;
                match s.resolve(item) {
                    Err(e) => return Err(e),
                    Ok(dt) => {
                        code.push_str(&dt);
                        if index < args.len() {
                            code.push_str(";\n");
                        }
                    }
                }
            }
            return Ok(format!("while (true) {{ {} }}", code));
        } else {
            if let Some(t) = args.get(index) {
                match &**t {
                    ParserTypes::Variable(v) => {
                        let mut base = String::new();
                        let mut executor = String::new();
                        index = index + 1;
                        if v == "for" {
                            let mut variable = String::new();
                            if let Some(v) = args.get(index) {
                                match s.resolve(v) {
                                Err(e) => return Err(e),
                                Ok(dt) => variable.push_str(&dt)
                                }
                            }

                            index = index + 1;
                            if let Some(ft) = args.get(index) {
                                match &**ft {
                                    ParserTypes::Variable(v) => {
                                        index = index + 1;

                                        if v == "from" {
                                            let mut min = String::new();
                                            let mut max = String::new();

                                            if let Some(m) = args.get(index) {
                                                match s.resolve(m) {
                                                    Err(e) => return Err(e),
                                                    Ok(dt) => min.push_str(&dt)
                                                }
                                            }
                                            index = index + 1;
                                            if let Some(t) = args.get(index) {
                                                match &**t {
                                                    ParserTypes::Variable(v) => {
                                                        if v != "to" {
                                                            return Err(Errors::Custom(String::from("Missing to key in for from loop")));
                                                        }
                                                    },
                                                    ParserTypes::Boolean(true) => {
                                                        index = index + 1;
                                                        if let Some(t) = args.get(index) {
                                                            match &**t {
                                                                ParserTypes::Variable(v) => {
                                                                    if v!="o" {
                                                                        return Err(Errors::Custom(String::from("Missing to key in for from loop")));
                                                                    }
                                                                },
                                                                _ => return Err(Errors::Custom(String::from("Missing to key in for from loop")))
                                                            }
                                                        }
                                                    }
                                                    _ => return Err(Errors::Custom(String::from("Missing to key in for from loop")))
                                                }
                                            }
                                            index=index+1;
                                            if let Some(m) = args.get(index) {
                                                match s.resolve(m) {
                                                    Err(e) => return Err(e),
                                                    Ok(dt) => max.push_str(&dt)
                                                }
                                            }
 

                                            index = index + 1;
                                            base.push_str(&format!("Array(Math.abs({} - {}) + 1).fill(1).map((_,i)=>({} - {} < 0)?{} + i:{} - i)", min, max, min, max, min, min));
                                            executor.push_str(&format!("({})=>{{ {{}} }}", variable));
                                        } else if v == "in" || v == "of" || v == "on" {
                                            let mut list = String::new();
                                            if let Some(l) = args.get(index) {
                                                match s.resolve(l) {
                                                    Err(e) => return Err(e),
                                                    Ok(dt) => list.push_str(&dt)
                                                }
                                            }
                                            index = index + 1;
                                            if v == "in" {
                                                executor.push_str(&format!("({})=>{{ {{}} }}", variable));
                                            } else if v == "of" {
                                                executor.push_str(&format!("(_, {})=>{{ {{}} }}", variable));
                                            } else if v == "on" {
                                                executor.push_str(&format!("(_, i, a)=>{{ \nconst {} = a.slice(i); \n{{}} }}", variable));
                                            }
                                            base.push_str(&list);
                                        } else {
                                            return Err(Errors::Custom(String::from("Invalid for loop type")));
                                        }
                                    },
                                    _ => return Err(Errors::Custom(String::from("Invalid for loop type")))
                                }
                            }

                        } else if v == "repeat" {
                            if let Some(c) = args.get(index) {
                                match s.resolve(c) {
                                    Err(e) => return Err(e),
                                    Ok(dt) => base.push_str(&format!("Array({}).fill(1).map((_,i)=>i)", &dt))
                                }
                                executor.push_str("i=>{{ {} }}");
                            }
                            index = index + 1;
                        } else {
                            return Err(Errors::Custom(String::from("Invalid loop type")));
                        }


                        let mut method = String::new();
                        if let Some(ct) = args.get(index) {
                            match &**ct {
                                ParserTypes::Variable(v) => {
                                    if v == "do" {
                                        method.push_str("forEach");
                                    } else if v == "collect" {
                                        method.push_str("map");
                                    } else {
                                        return Err(Errors::Custom(String::from("Invalid loop control type")))
                                    }
                                    index = index +1;
                                },
                                _ => return Err(Errors::Custom(String::from("Invalid loop control type")))
                            }
                        }

                        while let Some(cmd) = args.get(index) {
                            index = index + 1;
                            match s.resolve(cmd) {
                                Err(e) => return Err(e),
                                Ok(dt) => {
                                    code.push_str(&dt);
                                    if index < args.len() {
                                        code.push_str(";\n");
                                    }
                                }
                            }
                        }

                        return Ok(format!("{}.{}({})", base, method, executor.replace("{}", &code)));
                    },
                    _ => return Err(Errors::Custom(String::from("Invalid loop type")))
                }
            }
        }

        Err(Errors::Unknown)
    }));



    blueprint.register_command("print", Box::new(|s, args| {
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut builder = String::new();

        let mut index = 1;
        while let Some(arg) = args.get(index) {
            index+=1;
            match s.resolve(arg) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push(',');
                    }
                }
            }
        }

        Ok(format!("console.log({})", builder))
    }));
    blueprint.register_command("lambda", Box::new(|s, args| {
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut builder = String::new();
        let mut parameters = String::new();

        if let Some(arg) = args.get(1) {
            match &**arg {
                ParserTypes::Command(items) => {
                    for item in items {
                        match s.resolve(&*item) {
                            Err(e) => return Err(e),
                            Ok(dt) => parameters.push_str(&dt)
                        }
                        parameters.push(',');
                    }
                    parameters.pop();
                },
                _ => return Err(Errors::Unknown)
            }
        }

        let mut index = 2;
        while let Some(item) = args.get(index) {
            index+=1;
            match s.resolve(item) {
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push_str(";\n");
                    }
                },
                Err(e) => return Err(e)
            }
        }

        Ok(format!("({}) => {{ {} }}", parameters, builder))
    }));

    blueprint.register_command("multiple.values.bind", Box::new(|s, args| {
        if args.len() > 3 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut builder = String::new();
        let mut parameters = String::new();

        if let Some(arg) = args.get(1) {
            match &**arg {
                ParserTypes::Command(items) => {
                    for item in items {
                        match s.resolve(&*item) {
                            Err(e) => return Err(e),
                            Ok(dt) => parameters.push_str(&dt)
                        }
                        parameters.push(',');
                    }
                    parameters.pop();
                },
                _ => return Err(Errors::Unknown)
            }
        }

        if let Some(item) = args.get(2) {
            match s.resolve(item) {
                Ok(dt) => {
                    builder.push_str(&dt);
                },
                Err(e) => return Err(e)
            }
        }

        Ok(format!("let [{}] = {}", parameters, builder))
    }));

    blueprint.register_command("object.values.bind", Box::new(|s, args| {
        if args.len() > 3 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut builder = String::new();
        let mut parameters = String::new();

        if let Some(arg) = args.get(1) {
            match &**arg {
                ParserTypes::Command(items) => {
                    for item in items {
                        match s.resolve(&*item) {
                            Err(e) => return Err(e),
                            Ok(dt) => parameters.push_str(&dt)
                        }
                        parameters.push(',');
                    }
                    parameters.pop();
                },
                _ => return Err(Errors::Unknown)
            }
        }

        if let Some(item) = args.get(2) {
            match s.resolve(item) {
                Ok(dt) => {
                    builder.push_str(&dt);
                },
                Err(e) => return Err(e)
            }
        }

        Ok(format!("let {{ {} }} = {}", parameters, builder))
    }));

    // modules
    blueprint.register_command("dofile", Box::new(|s, args| {
        if args.len() > 3 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut modu = String::new();
        let mut cb = String::new();

        if let Some(arg) = args.get(1) {
            match s.resolve(arg) {
                Err(e) => return Err(e),
                Ok(dt) => modu.push_str(&dt)
            }
        }

        if let Some(item) = args.get(2) {
            match s.resolve(item) {
                Ok(dt) => {
                    cb.push_str(&dt);
                },
                Err(e) => return Err(e)
            }
        }

        Ok(format!("import({}).then({})", modu, cb))
    }));

    // array
    blueprint.register_command("append", Box::new(|s, args| {
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut builder = String::new();

        let mut index = 1;
        while let Some(item) = args.get(index) {
            index = index + 1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&format!("...{}", dt));
                    if index < args.len() {
                        builder.push(',');
                    }
                }
            }
        }

        Ok(format!("[{}]", builder))
    }));
    blueprint.register_command("push", Box::new(|s, args| {
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut ar = String::new();
        let mut builder = String::new();

        if let Some(a) = args.get(1) {
            match s.resolve(a) {
                Err(e) => return Err(e),
                Ok(dt) => ar.push_str(&dt)
            }
        }

        let mut index = 2;
        while let Some(item) = args.get(index) {
            index = index + 1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&format!("a1.push({});", dt));
                    if index < args.len() {
                        builder.push(',');
                    }
                }
            }
        }

        Ok(format!("((a1)=>{{ {} return a1 }})( [...{}] )", builder, ar))
    }));

    // errors 
    blueprint.register_command("error", Box::new(|s, args| {
        if args.len() < 1 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut builder = String::new();
        let mut index = 1;
        while let Some(item) = args.get(index) {
            index = index + 1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push(',');
                    }
                }
            }
        }

        Ok(format!("new Error({})", builder))
    }));
}
