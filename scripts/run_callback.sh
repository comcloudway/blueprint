#!/bin/bash
if [ -z $PRODUCTION ]; then
  cargo run -- -i "$1" -o "$2" -v -c "node $2"
else
  bpc -i "$1" -o "$2" -v -c "node $2"
fi
