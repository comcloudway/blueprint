use crate::lib::{BluePrint, Errors, ConverterFn, ParserTypes};

/// registers some default macros
pub fn register_macros(blueprint: &mut BluePrint) {
    blueprint.register_macro("format", Box::new(|s, args| {
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut base = String::new();
        if let Some(b) = args.get(1) {
            match s.resolve(b) {
                Err(e) => return Err(e),
                Ok(dt) => base.push_str(&dt)
            }
        };

        let mut index = 2;
        while let Some(item) = args.get(index) {
            index+=1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => base.push_str(&format!(".replace('{{}}', {})", &dt))
            }
        }

        Ok(base)
    }));

    // ARRAYS
    blueprint.register_macro("arr", Box::new(|s, args| {
        let mut builder = String::new();
        let mut index = 1;
        while let Some(item) = args.get(index) {
            index+=1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push(',');
                    }
                }
            }
        }
        Ok(format!("[{}]", builder))
    }));
    blueprint.register_macro("get", Box::new(|s, args| {
        if args.len() > 3 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut obj = String::new();
        let mut key = String::new();

        if let Some(o) = args.get(1) {
            match s.resolve(o) {
                Err(e) => return Err(e),
                Ok(dt) => obj.push_str(&dt)
            }
        }
        if let Some(k) = args.get(2) {
            match s.resolve(k) {
                Err(e) => return Err(e),
                Ok(dt) => key.push_str(&dt)
            }
        }

        Ok(format!("{}[{}]", obj, key))
    }));

    // JSON Objects
    blueprint.register_macro("obj", Box::new(|s, args| {
        let mut builder = String::new();
        let mut index = 1;
        let mut is_key = true;
        
        while let Some(item) = args.get(index) {
            index+=1;
            match s.resolve(item) {
                    Err(e) => return Err(e),
                    Ok(dt) => {
                        if is_key {
                            is_key=false;
                            builder.push_str(&dt);
                        } else {
                            is_key = true;
                            builder.push_str(&format!(": {},\n", dt));
                        }
                    }
            }
        }

        Ok(format!("{{ {} }}", builder))
    }));
}
