;; create 2 arrays
(let a1 (arr! 1 2 3))
(let a2 (arr! 4 5 6))

;; create a new array containing the two arrays
(let a3 (append a1 a2))

;; push 6 to a3 and create a new array
(let a4 (push a3 6))

;; change a4
(set a4 (push a4 7))

;; print a4
(print a4)
