; single path
(if (not nil)
  (print "Hi there"))
;; same as if
(when (not nil)
  (print "Hi there"))
; same as if but inverted
(unless nil
  (print "Hi there"))

; multi path
(cond
  ((not nil) (print "HI"))
  (t (print "elsish")))
