use crate::lib::{BluePrint, Errors, ConverterFn, ParserTypes};

/// registers some default keywords
pub fn register_keywords(blueprint: &mut BluePrint) {
    blueprint.register_keyword("let", Box::new(|s, args| {
        if args.len() > 3 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut name = String::new();
        let mut value = String::new();

        if let Some(n) = args.get(1) {
            match s.resolve(n) {
                Err(e) => return Err(e),
                Ok(dt) => name.push_str(&dt)
            }
        }
        if let Some(v) = args.get(2) {
            match s.resolve(v) {
                Err(e) => return Err(e),
                Ok(dt) => value.push_str(&dt)
            }
        }

        Ok(format!("let {} = {}", name, value))
    }));
    blueprint.register_keyword("const", Box::new(|s, args| {
        if args.len() > 3 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut name = String::new();
        let mut value = String::new();

        if let Some(n) = args.get(1) {
            match s.resolve(n) {
                Err(e) => return Err(e),
                Ok(dt) => name.push_str(&dt)
            }
        }
        if let Some(v) = args.get(2) {
            match s.resolve(v) {
                Err(e) => return Err(e),
                Ok(dt) => value.push_str(&dt)
            }
        }

        Ok(format!("const {} = {}", name, value))
    }));
    blueprint.register_keyword("var", Box::new(|s, args| {
        if args.len() > 3 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut name = String::new();
        let mut value = String::new();

        if let Some(n) = args.get(1) {
            match s.resolve(n) {
                Err(e) => return Err(e),
                Ok(dt) => name.push_str(&dt)
            }
        }
        if let Some(v) = args.get(2) {
            match s.resolve(v) {
                Err(e) => return Err(e),
                Ok(dt) => value.push_str(&dt)
            }
        }

        Ok(format!("var {} = {}", name, value))
    }));
    blueprint.register_keyword("return", Box::new(|s, args| {
        if args.len() > 2 {
            return Err(Errors::TooManyArguments)
        }
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut val = String::new();
        if let Some(a) = args.get(1) {
            match s.resolve(a) {
                Ok(dt) => val.push_str(&dt),
                Err(e) => return Err(e)
            }
        }

        Ok(format!("return {}", val))
    }));
    blueprint.register_keyword("break", Box::new(|s, args| {
        if args.len() > 2 {
            return Err(Errors::TooManyArguments)
        }
        if args.len() < 1 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut val = String::new();

        if let Some(a) = args.get(1) {
            match s.resolve(a) {
                Ok(dt) => val.push_str(&format!(" {}", &dt)),
                Err(e) => return Err(e)
            }
        }

        Ok(format!("break{}", val))
    }));
    blueprint.register_keyword("continue", Box::new(|s, args| {
        if args.len() > 1 {
            return Err(Errors::TooManyArguments)
        }
        if args.len() < 1 {
            return Err(Errors::NotEnoughArguments)
        }

        Ok(String::from("continue"))
    }));
    blueprint.register_keyword("new", Box::new(|s, args| {
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut class = String::new();
        let mut arg_builder = String::new();

        if let Some(c) = args.get(1) {
            match s.resolve(c) {
                Err(e) => return Err(e),
                Ok(dt) => class.push_str(&dt)
            }
        }
        let mut index = 2;
        while let Some(a) = args.get(index) {
            index+=1;
            match s.resolve(a) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    arg_builder.push_str(&dt);
                    if index < args.len() {
                        arg_builder.push(',');
                    }
                }
            }
        }

        Ok(format!("new {}({})", class, arg_builder))
    }));

    // basic math & comparisons
    for operator in ["+", ".", "/", "*", "%", "<", ">", "=", "<=", ">="] {
        blueprint.register_keyword(&operator.to_string(), Box::new(|s, args| {
            if args.len() < 3 {
                return Err(Errors::NotEnoughArguments)
            }
            let mut builder= String::new();
            let mut name = String::new();

            if let Some(c) = args.get(0) {
                match s.resolve(c) {
                    Err(e) => return Err(e),
                    Ok(dt) => name.push_str(&dt)
                }
            }

            let mut index = 1;
            while let Some(arg) = args.get(index) {
                match s.resolve(arg) {
                    Err(e) => return Err(e),
                    Ok(dt) => {
                        builder.push_str(&dt);

                        if index < args.len() - 1 {
                            builder.push_str(&name.replace(".", "-"));
                        }
                    }
                };
                index+=1;
            }

            Ok(format!("({})", builder))
        }));
    }

    // basic logic
    for (id, rp) in [("mod", "%"), ("and", "&&"), ("or", "||"), ("=", "=="), ("/=", "!=")] {
        blueprint.register_keyword(&id.to_string(), Box::new(move |s, args| {
            if args.len() < 3 {
                return Err(Errors::NotEnoughArguments)
            }
            let mut builder= String::new();
            let mut name = String::new();

            if let Some(c) = args.get(0) {
                match s.resolve(c) {
                    Err(e) => return Err(e),
                    Ok(dt) => name.push_str(&dt)
                }
            }

            let mut index = 1;
            while let Some(arg) = args.get(index) {
                match s.resolve(arg) {
                    Err(e) => return Err(e),
                    Ok(dt) => {
                        builder.push_str(&dt);

                        if index < args.len() - 1 {
                            builder.push_str(&rp.to_string());
                        }
                    }
                };
                index+=1;
            }

            Ok(format!("({})", builder))
        }));
    }
    blueprint.register_keyword("not", Box::new(move |s, args| {
        if args.len() > 2 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut exp = String::new();
        let mut name = String::new();

        if let Some(c) = args.get(0) {
            match s.resolve(c) {
                Err(e) => return Err(e),
                Ok(dt) => name.push_str(&dt)
            }
        }
        if let Some(c) = args.get(1) {
            match s.resolve(c) {
                Err(e) => return Err(e),
                Ok(dt) => exp.push_str(&dt)
            }
        }

        Ok(format!("!({})",exp))
    }));

    // Advanced Math tooling
    for id in ["pow", "sqrt", "sin", "cos", "tan", "asin", "acos", "atan", "abs", "max", "min"] {
        blueprint.register_keyword(&id.to_string(), Box::new(move |s, args| {
            if args.len() < 2 {
                return Err(Errors::NotEnoughArguments)
            }
            let mut builder= String::new();
            let mut name = String::new();

            if let Some(c) = args.get(0) {
                match s.resolve(c) {
                    Err(e) => return Err(e),
                    Ok(dt) => name.push_str(&dt)
                }
            }

            let mut index = 1;
            while let Some(arg) = args.get(index) {
                match s.resolve(arg) {
                    Err(e) => return Err(e),
                    Ok(dt) => {
                        builder.push_str(&dt);

                        if (index < args.len() - 1) {
                            builder.push(',');
                        }
                    }
                };
                index+=1;
            }

            Ok(format!("Math.{}({})", name, builder))
        }));
    }

    // Functions
    blueprint.register_keyword("defun", Box::new(|s, args| {
        if args.len() < 4 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut builder = String::new();
        let mut name = String::new();
        let mut parameters = String::new();

        if let Some(c) = args.get(1) {
            match s.resolve(c) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    name.push_str(&dt);
                }
            }
        }

        if let Some(arg) = args.get(2) {
            match &**arg {
                ParserTypes::Command(items) => {
                    for item in items {
                        match s.resolve(&*item) {
                            Err(e) => return Err(e),
                            Ok(dt) => parameters.push_str(&dt)
                        }
                        parameters.push(',');
                    }
                    parameters.pop();
                },
                _ => return Err(Errors::Unknown)
            }
        }

        let mut index = 3;
        while let Some(item) = args.get(index) {
            index+=1;
            match s.resolve(item) {
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push_str(";\n");
                    }
                },
                Err(e) => return Err(e)
            }
        }

        Ok(format!("function {}({}) {{ \n{} \n}}", name, parameters, builder))
    }));

    for id in ["if", "when"] {
        blueprint.register_keyword(id, Box::new(|s, args| {
            if args.len() < 3 {
                return Err(Errors::NotEnoughArguments)
            }
            let mut cond = String::new();
            let mut builder = String::new();

            if let Some(n) = args.get(1) {
                match s.resolve(n) {
                    Err(e) => return Err(e),
                    Ok(dt) => cond.push_str(&dt)
                }
            }

            let mut index = 2;
            while let Some(v) = args.get(index) {
                index = index + 1;
                match s.resolve(v) {
                    Err(e) => return Err(e),
                    Ok(dt) => {
                        builder.push_str(&dt);
                        if index < args.len() {
                            builder.push_str(";\n")
                        }
                    }
                }
            }

            Ok(format!("if ({}) {{ \n{} \n}}", cond, builder))
        }));
    }
    blueprint.register_keyword("unless", Box::new(|s, args| {
        if args.len() < 3 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut cond = String::new();
        let mut builder = String::new();

        if let Some(n) = args.get(1) {
            match s.resolve(n) {
                Err(e) => return Err(e),
                Ok(dt) => cond.push_str(&dt)
            }
        }

        let mut index = 2;
        while let Some(v) = args.get(index) {
            index = index + 1;
            match s.resolve(v) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push_str(";\n")
                    }
                }
            }
        }

        Ok(format!("if (!({})) {{ \n{} \n}}", cond, builder))
    }));
    blueprint.register_keyword("cond", Box::new(|s, args| {
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments)
        }
        let mut builder = String::new();

        let mut index = 1;
        while let Some(v) = args.get(index) {
            match &**v {
                ParserTypes::List(l) => {
                    let mut cond = String::new();
                    let mut code = String::new();

                    if let Some(c) = l.get(0) {
                        match s.resolve(c) {
                            Err(e) => return Err(e),
                            Ok(dt) => cond.push_str(&dt)
                        }
                    }

                    let mut i = 1;
                    while let Some(item) = l.get(i) {
                        i = i + 1;
                        match s.resolve(item) {
                            Err(e) => return Err(e),
                            Ok(dt) => {
                                code.push_str(&dt);
                                if i < l.len() {
                                    code.push_str(";\n");
                                }
                            }
                        }
                    }

                    builder.push_str(
                        &format!("{} ({}) {{ \n{} \n}}",
                                 if index == 1 {"if"} else {"else if"},
                                 &cond,
                                 &code));
                },
                _ => return Err(Errors::InvalidType)
            }

            index = index + 1;
        }

        Ok(builder)
    }));

    // modules
    blueprint.register_keyword("export", Box::new(|s, args| {
        if args.len() > 2 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut val = String::new();

        if let Some(arg) = args.get(1) {
            match s.resolve(arg) {
                Err(e) => return Err(e),
                Ok(dt) => val.push_str(&dt)
            }
        }

        Ok(format!("export {}", val))
    }));
    blueprint.register_keyword("export.default", Box::new(|s, args| {
        if args.len() > 2 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 2 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut val = String::new();

        if let Some(arg) = args.get(1) {
            match s.resolve(arg) {
                Err(e) => return Err(e),
                Ok(dt) => val.push_str(&dt)
            }
        }

        Ok(format!("export default {}", val))
    }));
    blueprint.register_keyword("import", Box::new(|s, args| {
        if args.len() > 6 {
            return Err(Errors::TooManyArguments);
        }
        if args.len() < 4 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut sel = String::new();
        let mut path = String::new();

        if let Some(arg) = args.get(1) {
            match s.resolve(arg) {
                Err(e) => return Err(e),
                Ok(dt) => sel.push_str(&dt)
            }
        }


        if let Some(arg) = args.get(if args.len() == 6 {5} else {3}) {
            match s.resolve(arg) {
                Err(e) => return Err(e),
                Ok(dt) => path.push_str(&dt)
            }
        }

        if args.len() == 6 {
            let mut a = String::new();

            if let Some(arg) = args.get(3) {
                match s.resolve(arg) {
                    Err(e) => return Err(e),
                    Ok(dt) => a.push_str(&dt)
                }
            }

            Ok(format!("import {} as {} from {}", sel, a, path))
        } else {
            Ok(format!("import {} from {}", sel, path))
        }
    }));

    // errors
    blueprint.register_keyword("throw", Box::new(|s, args| {
        if args.len() < 1 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut builder = String::new();
        let mut index = 1;
        while let Some(item) = args.get(index) {
            index = index + 1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push(',');
                    }
                }
            }
        }

        Ok(format!("throw new Error({})", builder))
    }));
    blueprint.register_keyword("try", Box::new(|s, args| {
        if args.len() < 1 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut builder = String::new();
        let mut index = 1;
        while let Some(item) = args.get(index) {
            index = index + 1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push_str(";\n");
                    }
                }
            }
        }

        Ok(format!("try {{ \n{} \n}}", builder))
    }));
    blueprint.register_keyword("catch", Box::new(|s, args| {
        if args.len() < 1 {
            return Err(Errors::NotEnoughArguments);
        }
        let mut e = String::new();
        let mut builder = String::new();

        if let Some(arg) = args.get(1) {
            match &**arg {
                ParserTypes::Command(items) => {
                    for item in items {
                        match s.resolve(&*item) {
                            Err(e) => return Err(e),
                            Ok(dt) => e.push_str(&dt)
                        }
                        e.push(',');
                    }
                    e.pop();
                },
                _ => return Err(Errors::Unknown)
            }
        }

        let mut index = 2;
        while let Some(item) = args.get(index) {
            index = index + 1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push_str(";\n");
                    }
                }
            }
        }

        Ok(format!("}} catch ({}) {{ \n{}", e, builder))
    }));
    blueprint.register_keyword("finally", Box::new(|s, args| {
        let mut builder = String::new();
        let mut index = 1;
        while let Some(item) = args.get(index) {
            index += 1;
            match s.resolve(item) {
                Err(e) => return Err(e),
                Ok(dt) => {
                    builder.push_str(&dt);
                    if index < args.len() {
                        builder.push(',');
                    }
                }
            }
        }

        Ok(format!("}} finally {{ \n{}", builder))
    }));

    // classes
    blueprint.register_keyword("defclass", Box::new(|s, args| {
        if let Some(name) = args.get(1) {
            if let ParserTypes::Variable(class_name) = &**name {

                if let Some(extends) = args.get(2) {
                    if let ParserTypes::Command(class_extends) = &**extends {
                        let class_extends = if class_extends.is_empty() {
                            String::new()
                        } else if let ParserTypes::Variable(class) = &**class_extends.first().unwrap() {
                            format!("extends {}", class)
                        } else {
                            String::new()
                        };

                        let mut builder = String::new();
                        let mut index = 3;
                        while let Some(item) = args.get(index) {
                            index += 1;
                            if let ParserTypes::Command(dataset) = &**item {
                                enum FieldType {
                                    Setter,
                                    Getter,
                                    Variable,
                                    Function,
                                    Constructor
                                }
                                impl FieldType {
                                    pub fn from(s: &str) -> Option<Self> {
                                        match s {
                                            "set" => Some(Self::Setter),
                                            "get" => Some(Self::Getter),
                                            "dyn" => Some(Self::Variable),
                                            "func" => Some(Self::Function),
                                            "constructor" => Some(Self::Constructor),
                                            _ => None
                                        }
                                    }
                                }


                                let mut is_static = false;
                                let mut field_type: Option<FieldType> = None;
                                let mut list_index = 0;
                                while field_type.is_none() {
                                    if let Some(item) = dataset.get(list_index) {
                                        list_index+=1;
                                        if let ParserTypes::Variable(name) = &**item {
                                            if let Some(ft) = FieldType::from(name) {
                                                field_type = Some(ft);
                                            } else if name == "static" {
                                                is_static = true
                                            } else {
                                                break
                                            }
                                        } else {
                                            break
                                        }
                                    } else {
                                        break
                                    }
                                }

                                if let Some(t) = field_type {
                                    let mut local_builder = String::from(if is_static {"static "} else {""});

                                    match t {
                                        FieldType::Variable => {
                                            if let Some(name) = dataset.get(list_index) {
                                                list_index+=1;
                                                if let ParserTypes::Variable(name) = &**name {
                                                    if let Some(value) = dataset.get(list_index) {
                                                        if let Ok(value) = s.resolve(value) {
                                                            local_builder.push_str(&format!("{} = {}", name.replace('.', "_"), value));
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        t => {
                                            let prefix = match t {
                                                FieldType::Constructor => "constructor ",
                                                FieldType::Getter => "get ",
                                                FieldType::Setter => "set ",
                                                _ => ""
                                            };
                                            let name = match t {
                                                FieldType::Constructor | FieldType::Variable => String::new(),
                                                _ => {
                                                    if let Some(d) = dataset.get(list_index) {
                                                        list_index+=1;
                                                        s.resolve(d).unwrap().replace('.', "_")
                                                    } else {
                                                        String::new()
                                                    }
                                                }
                                            };

                                            if let Some(f) = dataset.get(list_index) {
                                                if let ParserTypes::List(dt) = &**f {
                                                    let mut params = String::new();
                                                    let mut block = String::new();
                                                    let mut index = 0;

                                                    while let Some(arg) = dt.get(index) {
                                                        if index == 0 {
                                                            // params
                                                            match &**arg {
                                                                ParserTypes::Command(items) => {
                                                                    for item in items {
                                                                        match s.resolve(&*item) {
                                                                            Err(e) => return Err(e),
                                                                            Ok(dt) => params.push_str(&dt)
                                                                        }
                                                                        params.push(',');
                                                                    }
                                                                    params.pop();
                                                                },
                                                                _ => return Err(Errors::Unknown)
                                                            }
                                                        } else {
                                                            // code
                                                            while let Some(item) = dt.get(index) {
                                                                index+=1;
                                                                match s.resolve(item) {
                                                                    Ok(dt) => {
                                                                        block.push_str(&dt);
                                                                        if index < args.len() {
                                                                            block.push_str(";\n");
                                                                        }
                                                                    },
                                                                    Err(e) => return Err(e)
                                                                }}}
                                                        index+=1;
                                                    }

                                                    local_builder.push_str(&format!("{}{}({}) {{ {} }}", prefix, name, params, block));
                                                }
                                            }
                                        }
                                    };

                                    builder.push('\n');
                                    builder.push_str(&local_builder);
                                }
                            }
                        }


                        return Ok(format!("class {} {} {{ \n{} }}", class_name, class_extends, builder))
                    }
                }
            }
        }
        Err(Errors::InvalidType)
    }))
}
