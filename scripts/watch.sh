#!/bin/bash
# watches input file and runs $3 on change
# loop 
while [ True ]; do
  # watch
  inotifywait -q -r -t 0 -m -e attrib $1 | $3 $1 $2
done
