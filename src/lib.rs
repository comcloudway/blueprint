use std::collections::HashMap;

pub type ConverterFn = Box<dyn Fn(&BluePrint, Vec<Box<ParserTypes>>)->Result<String, Errors>>;

/// Construct for templated commands
pub struct TemplateCommand {
    /// command name
    pub name: String,
    /// exact argument count
    pub len: usize,
    /// template string - used for replacement
    pub template: String
}

/// Valid Error Codes
#[derive(Debug)]
pub enum Errors {
    /// error apurce unknown
    Unknown,
    /// Not enough arguments
    NotEnoughArguments,
    /// too many arguments
    TooManyArguments,
    /// Invalid type
    InvalidType,
    /// Custom error messages
    Custom(String)
}

/// Types used by Lexer to represent Code
#[derive(Debug, Clone)]
pub enum LexerTypes {
    /// '<text> -> Symbol(<text>)
    SingleQuote,
    /// :<key> represents a key
    Colon,
    /// new context 
    OpeningBracket(usize),
    /// end of current context
    ClosingBracket(usize),
    /// simplify spaces & lines
    Empty,
    /// Integer -> Number
    Integer(isize),
    /// Floating pont number
    Float(f64),
    /// Text section
    Text(String),
    /// Variable name
    Variable(String),
    /// nil->false, t->true
    Boolean(bool),
    /// Comment (;<comment> -> //<comment>)
    Comment(String),
    /// Multiline Comment (#||<comment>||# -> /*<comment>*/)
    MultilineComment(String)
}

/// Types used by Parser & Compiler to resolve types
#[derive(Debug, Clone)]
pub enum ParserTypes {
    /// Keyword
    Keyword(Vec<Box<ParserTypes>>),
    /// Macro -> extends to custom js
    Macro(Vec<Box<ParserTypes>>),
    /// Command (data collection)
    Command(Vec<Box<ParserTypes>>),
    /// Integer -> Number
    Integer(isize),
    /// Floating pont number
    Float(f64),
    /// Text section
    Text(String),
    /// Variable name
    Variable(String),
    /// A key written in lisp as :<name>
    Key(String),
    /// nil->false, t->true
    Boolean(bool),
    /// Comment (;<comment> -> //<comment>)
    Comment(String),
    /// Multiline Comment (#||<comment>||# -> /*<comment>*/)
    MultilineComment(String),
    /// ' -> new Symbol
    Symbol(String),
    /// List -> []
    List(Vec<Box<ParserTypes>>)
}
impl ParserTypes {
    /// tries to convert a lexer type to a parser type, if a parallel type exists
    pub fn from_lexer_type(lx: LexerTypes) -> Option<ParserTypes> {
        match lx {
            LexerTypes::Variable(d) => Some(ParserTypes::Variable(d)),
            LexerTypes::Integer(i) => Some(ParserTypes::Integer(i)),
            LexerTypes::Float(f) => Some(ParserTypes::Float(f)),
            LexerTypes::Text(t) => Some(ParserTypes::Text(t)),
            LexerTypes::Boolean(b) => Some(ParserTypes::Boolean(b)),
            LexerTypes::Comment(c) => Some(ParserTypes::Comment(c)),
            LexerTypes::MultilineComment(c) => Some(ParserTypes::MultilineComment(c)),
            _=>None
        }
    }
}

/// BluePrint Compiler Command Context
pub struct BluePrint {
    keywords: HashMap<String, ConverterFn>,
    macros: HashMap<String, ConverterFn>,
    commands: HashMap<String, ConverterFn>
}
impl BluePrint {
    /// initialises a new Compiler context
    pub fn new() -> BluePrint {
        BluePrint {
            keywords: HashMap::new(),
            macros: HashMap::new(),
            commands: HashMap::new()
        }
    }
    /// Adds a new Keyword conversion function
    pub fn register_keyword(&mut self, name:&str, func: ConverterFn) {
        self.keywords.insert(name.to_string(), func);
    }
    /// Adds a new Macrro conversion function
    pub fn register_macro(&mut self, name:&str, func: ConverterFn) {
        self.macros.insert(format!("{}!", name).to_string(), func);
    }
    /// Adds a new Command conversion function
    pub fn register_command(&mut self, name:&str, func: ConverterFn) {
        self.commands.insert(name.to_string(), func);
    }

    /// Adds a new command conversion function by using the string template syntax and a provided argument length
    /// no need to do programming anymore,
    /// now you can autogenerate nearly everything
    pub fn register_templated_command(&mut self, cmd: &TemplateCommand) {
        let name = cmd.name.to_string();
        let len = cmd.len + 1; // function name counts as argument
        let template = (&cmd.template).to_string();
        self.commands.insert(name.to_string(), Box::new(move |s, args| {
            if args.len() < len {
                return Err(Errors::NotEnoughArguments);
            } else if args.len() > len {
                return Err(Errors::TooManyArguments);
            }

            let mut builder = String::from(&template);

            for a in 0..len {
                if let Some(arg) = args.get(a) {
                    match s.resolve(arg) {
                        Ok(dt) => {
                            builder=builder.replace(&format!("{{ {} }}", a), &dt);
                        }
                        Err(e)=>return Err(e)
                    }
                } else {
                    return Err(Errors::NotEnoughArguments)
                }
            }

            Ok(builder)
        }));
    }
    // lexer taken from my lisp interpreter >>cracker<< over at https://codeberg.org/comcloudway/cracker
    /// lexes the given code
    pub fn lex(& self, code: &str) -> Vec<LexerTypes> {
        let mut buffer: Vec<LexerTypes> = Vec::new();

        let mut index:usize = 0;
        let code = code.to_owned()+" ";
        let chars: Vec<_> = code.chars().collect();
        let len = chars.len();
        let mut brackets:usize = 0;

        while index < len {
            let chars:Vec<_> = code.chars().collect();
            match chars.get(index) {
                Some(c) => {
                    let mut done = false;
                    if buffer.len() >0 {
                        done = match buffer.get(buffer.len()-1) {
                            Some(node) => match node {
                                LexerTypes::Empty => *c==' ', //removes empty duplicates
                                LexerTypes::Text(cur) => {
                                    let mut end:Vec<char> = Vec::new();
                                    if buffer.len()>1 {
                                        match buffer.get(buffer.len()-2) {
                                            Some(node)=>match node {
                                                LexerTypes::SingleQuote=>{
                                                    end.push(' ');
                                                    end.push('\n');
                                                    end.push(')');
                                                },
                                                LexerTypes::Colon => {
                                                    end.push(' ');
                                                },
                                                _=>end.push('"')
                                            },
                                            None=>end.push('"')
                                        };
                                    } else {
                                        end.push('"');
                                    }

                                    let mut is_escaped = false;
                                    if index > 0 {
                                        is_escaped = match chars.get(index-1) {
                                            Some(c) => *c=='\\',
                                            None => false
                                        }
                                    }

                                    if end.contains(&c) && !is_escaped {
                                        // end string
                                        buffer.push(LexerTypes::Empty);
                                        if *c==')' {
                                            false
                                        } else {
                                            true 
                                        }
                                    } else {
                                        // append to string
                                        let i = buffer.len() - 1;
                                        buffer.insert(i, LexerTypes::Text(cur.to_owned()+&c.to_string()));
                                        buffer.remove(i+1);
                                        true
                                    }
                                },
                                LexerTypes::Comment(cur) => {
                                    if *c == '\n' {
                                                // line break -> end of comment
                                                buffer.push(LexerTypes::Empty);
                                                continue;
                                    } else if c != &';' {
                                    let i = buffer.len() - 1;
                                    buffer.insert(i, LexerTypes::Comment(cur.to_owned()+&c.to_string()));
                                    buffer.remove(i+1);
                                    };
                                    true
                                }
                                LexerTypes::MultilineComment(cur) => {
                                    if *c == '|' {
                                        match code.chars().nth(index+1) {
                                            Some(c) => if c == '#' {
                                                // !# -> end of comment
                                                buffer.push(LexerTypes::Empty);
                                                index+=2;
                                                continue;
                                            },
                                            None => ()
                                        }
                                    };
                                    let i = buffer.len() - 1;
                                    buffer.insert(i, LexerTypes::MultilineComment(cur.to_owned()+&c.to_string()));
                                    buffer.remove(i+1);
                                    true
                                }
                                _=>false
                            },
                            None=>false
                        };
                    }
                    if !done {
                        match c {
                            '(' => {
                                buffer.push(LexerTypes::OpeningBracket(0+brackets));
                                brackets+=1;
                            },
                            ')' => {
                                if brackets <1 {
                                    return buffer;
                                }
                                brackets-=1;
                                buffer.push(LexerTypes::ClosingBracket(0+brackets));
                            },
                            ' ' => buffer.push(LexerTypes::Empty),
                            '"' => buffer.push(LexerTypes::Text(String::new())),
                            '\n' => buffer.push(LexerTypes::Empty),
                            '\'' => {
                                buffer.push(LexerTypes::SingleQuote);
                                match chars.get(index+1) {
                                    Some(cr) => {
                                        match cr {
                                        '(' =>(),
                                        _=>buffer.push(LexerTypes::Text(String::new()))
                                        }},

                                    None=>buffer.push(LexerTypes::Text(String::new()))
                                }
                            },
                            ':' => {
                                buffer.push(LexerTypes::Colon);
                            },
                            ';' => buffer.push(LexerTypes::Comment(String::new())),
                            _=>{
                                // variable & number detection
                                // build
                                let mut builder = String::new();
                                let mut end = 0;
                                for (i,c) in code.chars().enumerate() {
                                    if i>=index {
                                        if vec!['\\', ' ', ')', '\n'].contains(&c) {
                                            // close builder
                                            end=i;
                                            break;
                                        }
                                        builder.push(c);
                                    }
                                };

                                if builder == "nil".to_string() {
                                    buffer.push(LexerTypes::Boolean(false))
                                } else if builder  == "#|" {
                                    buffer.push(LexerTypes::MultilineComment(String::new()))
                                } else if builder == "t" {
                                    buffer.push(LexerTypes::Boolean(true));
                                }else {
                                    let mut res_int:isize = 0;
                                    let mut res_float:f64 = 0.0;


                                    // try parsing values;
                                    let is_int = match builder.parse::<isize>() {
                                        Ok(num) => {
                                            res_int=num;
                                            true
                                        },
                                        Err(_) => false
                                    };
                                    let is_float = match builder.parse::<f64>() {
                                        Ok(num) => {
                                            res_float = num;
                                            true
                                        },
                                        Err(_) => false
                                    };

                                    // results
                                    if is_float && !is_int {
                                        buffer.push(LexerTypes::Float(res_float));
                                    } else if is_int {
                                        buffer.push(LexerTypes::Integer(res_int));
                                    } else {
                                        // is variable
                                        buffer.push(LexerTypes::Variable(builder.replace("-", ".")));
                                    }
                                };
                                index=end;
                                continue;
                            }
                        }
                    }
                },
                None => break
            }
            index+=1;
        }

        //remove Empty
        buffer.retain(|b| match b {LexerTypes::Empty => false, _=>true});

        buffer
    }

    /// parses the given lexed code
    pub fn parse(& self, lexed: Vec<LexerTypes>) -> Result<Vec<ParserTypes>, Errors> {
        let mut root: Vec<ParserTypes> = Vec::new();

        // indicates the context level
        let mut level = 0;
        let mut level_data:HashMap<usize, Vec<Box<ParserTypes>>> = HashMap::new();
        let mut is_list:HashMap<usize, bool> = HashMap::new();

        // loop over lexed data
        let mut index = 0;
        while index < lexed.len() {
            if let Some(entry) = lexed.get(index) {
                match entry {
                    LexerTypes::OpeningBracket(l) => {
                        // next level
                        level_data.insert(*l, Vec::new());
                        level = *l;
                    },
                    LexerTypes::ClosingBracket(l) => {
                        if let Some(v) = level_data.get(l) {
                            let mut component: Option<ParserTypes> = None;
                            if let Some(_) = is_list.get(l) {
                                // list
                                    component=Some(ParserTypes::List(v.to_vec()));
                                    is_list.remove(l);
                            } else {
                                // command of some sort
                                if let Some(item) = v.get(0) {
                                    match &**item {
                                        ParserTypes::Variable(var) => {
                                            if self.macros.contains_key(var) {
                                                // macro
                                                component=Some(ParserTypes::Macro(v.to_vec()));
                                            } else if self.keywords.contains_key(var) {
                                                // keyword
                                                component=Some(ParserTypes::Keyword(v.to_vec()));
                                            } else {
                                                // command
                                                component=Some(ParserTypes::Command(v.to_vec()));
                                            }
                                        },
                                        _=>{
                                            component=Some(ParserTypes::List(v.to_vec()));
                                            //return Err(Errors::InvalidType);
                                        }
                                    }
                                } else {
                                    component=Some(ParserTypes::Command(v.to_vec()));
                                }
                            }
                            if let Some(c) = component {
                                if l > &0 {
                                    match level_data.get_mut(&(l-1)) {
                                        Some(vec) => vec.push(Box::new(c)),
                                        None => ()
                                    }
                                } else {
                                    root.push(c);
                                }
                            }
                        }
                        if l > &0 {
                            level = l - 1;
                        }
                    },
                    LexerTypes::SingleQuote => {
                        match lexed.get(index+1) {
                            Some(s)=>match s {
                                LexerTypes::Text(t) => {
                                    index=index+1;
                                    if let Some(vec) = level_data.get_mut(&level) {
                                        vec.push(Box::new(ParserTypes::Symbol(t.to_string())));
                                    }
                                },
                                LexerTypes::OpeningBracket(l) => {
                                    is_list.insert(*l, true); 
                                },
                                _ => {
                                    return Err(Errors::InvalidType);
                                }
                            },
                            None => ()
                        }
                    },
                    LexerTypes::Colon => {
                        match lexed.get(index+1) {
                            Some(s)=>{
                                if let LexerTypes::Text(t) = s {
                                    index = index + 1;
                                    if let Some(vec) = level_data.get_mut(&level) {
                                        vec.push(Box::new(ParserTypes::Key(t.to_string())));
                                    }
                                }
                            },
                            None=>()
                        }
                    },
                    d => {
                        match level_data.get_mut(&level) {
                            Some(vec) => {
                                if let Some(dt) = ParserTypes::from_lexer_type(d.clone()) {
                                    vec.push(Box::new(dt));
                                }
                            },
                            None => ()
                        }
                    }
                }
            }
            index=index+1;
        }

        Ok(root)
    }

    /// resolves the given values
    pub fn resolve(&self, pr: &ParserTypes) -> Result<String, Errors> {
        let mut buffer = String::new();

        match pr {
            ParserTypes::Variable(v) => {
                buffer.push_str(&v.replace("-", "."));
            },
            ParserTypes::Command(c) => {
                if let Some(cmd) = c.get(0) {
                    match &**cmd {
                        ParserTypes::Variable(name) => {
                            match self.commands.get(name) {
                                Some(f) => match f(self, c.to_vec()) {
                                    Err(e) => return Err(e),
                                    Ok(dt) => buffer.push_str(&dt)
                                }
                                // default implementation
                                None => match self.commands.get("") {
                                    Some(f) => match f(self, c.to_vec()) {
                                        Err(e) => return Err(e),
                                        Ok(dt) => buffer.push_str(&dt)
                                    },
                                    None => ()
                                }

                            }
                        },
                        // invalid type
                        _=>{
                            return Err(Errors::InvalidType);
                        }
                    }
                }
            },
            ParserTypes::Macro(m) => {
                if let Some(mac) = m.get(0) {
                    match &**mac {
                        ParserTypes::Variable(name) => {
                            if let Some(f) = self.macros.get(name) {
                                match f(self, m.to_vec()) {
                                    Err(e) => return Err(e),
                                    Ok(dt) => buffer.push_str(&dt)
                                }
                            }
                        },
                        // invalid type
                        _=>{
                            return Err(Errors::InvalidType);
                        }
                    }
                }
            },
            ParserTypes::Keyword(k) => {
                if let Some(key) = k.get(0) {
                    match &**key {
                        ParserTypes::Variable(name) => {
                            if let Some(f) = self.keywords.get(name) {
                                match f(self, k.to_vec()) {
                                    Err(e) => return Err(e),
                                    Ok(dt) => buffer.push_str(&dt)
                                }
                            }
                        },
                        // invalid type
                        _=>{
                            return Err(Errors::InvalidType);
                        }
                    }
                }
            },
            ParserTypes::Text(t) => {
                buffer.push_str(&format!("\"{}\"", t));
            },
            ParserTypes::Float(n) => {
                buffer.push_str(&format!("{}", n));
            },
            ParserTypes::Integer(n) => {
                buffer.push_str(&format!("{}", n));
            },
            ParserTypes::Boolean(b) => {
                buffer.push_str(&format!("{}", b));
            },
            ParserTypes::Comment(c)=>{
                buffer.push_str(&format!("//{}\n", c));
            },
            ParserTypes::MultilineComment(c)=>{
                buffer.push_str(&format!("/*{}*/\n", c));
            },
            ParserTypes::Symbol(t) => {
                buffer.push_str(&format!("Symbol(\"{}\")", t));
            },
            ParserTypes::List(l) => {
                let mut builder = String::new();
                let mut index = 0;
                while let Some(item) = l.get(index) {
                    index = index + 1;
                    match self.resolve(item) {
                        Err(e) => return Err(e),
                        Ok(dt) => {
                            builder.push_str(&dt);
                            if index < l.len() {
                                builder.push(',');
                            }
                        }
                    }
                }
                buffer.push_str(&format!("[{}]", builder));
            },
            _ => ()
        };

        Ok(buffer)
    }

    /// converts the given parsed code
    pub fn convert(&self, pr: Vec<ParserTypes>) -> Result<String,Errors> {
        let mut buffer = String::new();

        let mut index = 0;
        while index < pr.len() {
            if let Some(p) = pr.get(index) {
                match self.resolve(p) {
                    Ok(dt) => buffer.push_str(&dt),
                    Err(e) => return Err(e)
                }
            }
            index=index+1;
        };

        Ok(buffer)
    }

    /// automatically lex, parse & convert code
    pub fn compile(&self, lisp: &str) -> Result<String, Errors> {
        let lx = self.lex(lisp);
        let pr = self.parse(lx);
        match pr {
            Err(e) => return Err(e),
            Ok(r) => {
                let cv = self.convert(r);
                return cv;
            }
        }
    }
}
