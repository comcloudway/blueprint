use crate::lib::{BluePrint, Errors, ConverterFn, ParserTypes};

/// registers the default command formatter
pub fn register_auto_commands(blueprint: &mut BluePrint) {
    blueprint.register_command("", Box::new(|s, args| {
        let mut builder = String::new();
        let mut name = String::new();

        if let Some(n) = args.get(0) {
            match s.resolve(n) {
                Ok(dt) => name.push_str(&dt),
                Err(e) => return Err(e)
            }
        }

        // skip command name
        let mut index = 1;
        while let Some(d) = args.get(index) {
            index+=1;
            match s.resolve(d) {
                Ok(dt) => builder.push_str(&dt),
                Err(e) => return Err(e)
            }
            if index<args.len() {
                builder.push_str(", ");
            }
        }
        Ok(format!("{}({})", name, builder))
    }));
}
