(defclass person ()
  (:constructor ((name age)
                 (set this-name name)
                 (set this-age age)))
  (:dyn weight 50)
  (:get get-name (()
                  (return this-name)))
  (:set birthdays ((count)
                   (set this-age (+ this-age count))))
  (:static :get has-bones (()
                           (return t)))
  (:func eat ((food)
              (print "Jummy")))
  )

(let tom (make-instance 'person
                         "Tom"
                         26))

(print tom)
